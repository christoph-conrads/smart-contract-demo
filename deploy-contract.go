// Copyright 2019 Christoph Conrads
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import "context"
import contracts "./contracts"
import "crypto/ecdsa"
import "fmt"
import "github.com/ethereum/go-ethereum/ethclient"
import "github.com/ethereum/go-ethereum/accounts/abi/bind"
import "github.com/ethereum/go-ethereum/accounts/keystore"
import "github.com/ethereum/go-ethereum/crypto"
import "io/ioutil"
import "log"
import "math/big"
import "os"


func main() {
	args := os.Args

	if len(args) != 3 {
		fmt.Fprintf(
			os.Stderr,
			"Usage: %s <private key file> <key password>\n",
			args[0])
		os.Exit(1)
	}


	keyFile := args[1]
	password := args[2]


	// load key pair
	serializedKey, err := ioutil.ReadFile(keyFile)

	if err != nil {
		log.Fatal(err)
	}

	key, err := keystore.DecryptKey(serializedKey, password)

	if err != nil {
		log.Fatal(err)
	}

	privateKey := key.PrivateKey
	publicKey, ok := privateKey.Public().(*ecdsa.PublicKey)

	if !ok {
		log.Fatal("Cannot cast public key to type ecdsa.PublicKey")
	}

	address := crypto.PubkeyToAddress(*publicKey)

	fmt.Printf("Loaded account with address %s\n", address.String())



	// connect to an ETH network
	gatewayUrl := "wss://rinkeby.infura.io/ws"
	client, err := ethclient.Dial(gatewayUrl)

	if err != nil {
		log.Fatal(err)
	}


	// deploy contract
	nonce, err := client.PendingNonceAt(context.Background(), address)

	if err != nil {
		log.Fatal(err)
	}

	gasPrice, err := client.SuggestGasPrice(context.Background())

	if err != nil {
		log.Fatal(err)
	}

	auth := bind.NewKeyedTransactor(privateKey)
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)
	auth.GasLimit = uint64(300 * 1000)
	auth.GasPrice = gasPrice

	initial_value := int64(-1)
	address, tx, _, err := contracts.DeploySimpleContract(auth, client, initial_value)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(
		"Contract deployed at address %s in transaction %s\n",
		address.Hex(), tx.Hash().Hex()[:10])
}
