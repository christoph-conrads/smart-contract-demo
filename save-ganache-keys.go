// Copyright 2019 Christoph Conrads
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import "fmt"
import "github.com/ethereum/go-ethereum/accounts/keystore"
import "github.com/miguelmota/go-ethereum-hdwallet"
import "log"
import "os"



func main() {
	args := os.Args

	if len(args) != 3 {
		fmt.Fprintf(
			os.Stderr,
			"Usage: %s <wallet directory> <password>\n",
			args[0])
		os.Exit(1)
	}

	walletDirectory := args[1]
	password := args[2]


	// derive keys
	mnemonic := "quick area hollow hint punch vocal defy tonight key forward safe liquid"
	wallet, err := hdwallet.NewFromMnemonic(mnemonic)

	if err != nil {
		log.Fatal(err)
	}

	accountPath := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/0")
	account, err := wallet.Derive(accountPath, false)

	if err != nil {
		log.Fatal(err)
	}


	// account.Address
	privateKey, err := wallet.PrivateKey(account)

	if err != nil {
		log.Fatal(err)
	}


	// save key in keystore
	ks := keystore.NewKeyStore(
		walletDirectory, keystore.StandardScryptN, keystore.StandardScryptP)

	ksAccount, err := ks.ImportECDSA(privateKey, password)

	if err != nil {
		log.Fatal(err)
	}

	if ksAccount.Address != account.Address {
		log.Fatal("Error when moving private key into wallet")
	}

	println("Saved private key of", account.Address.Hex())
}
