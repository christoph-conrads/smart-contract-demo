# Smart Contract Demo

This repository contains a simple demonstration for deployment and modification of a smart contract on an Ethereum test network. The demo is written in golang, Ethereum's smart contract language Solidity, and uses the go-ethereum package.

To build the code, run `make`. It is assumed that the following programs and packages are installed:
* `go` (the golang package) and
* [go-ethereum](https://github.com/ethereum/go-ethereum) including the programs
  * `abigen`,
  * `solc`.

First, create an Ethereum account by creating a public/private key pair. The program `make-private-key.go` does this for you. Think of a phrase, a password, and decide on an existing folder where you want to store the key pair. In this README, the phrase is `cryptocurrency`, the password is `password`, and the key is saved in the current working directory. Run the program:
```
$ ./make-private-key.go 'cryptocurrency' . 'password'
Created account with address 0xAa84041541030E1A3cA1dbDf39Ac54e3341C309F
```
Note the account address. There should be a new file in the current working directory with a long name like `UTC--2019-07-06T17-05-02.172365851Z--aa84041541030e1a3ca1dbdf39ac54e3341c309f`. Rename it if you want to:
```sh
mv UTC--2019-07-06T17-05-02.172365851Z--aa84041541030e1a3ca1dbdf39ac54e3341c309f 0xaa840415.key
```
Next, get some Ethereum to spend, e.g., from the [Rinkeby Faucet](https://faucet.rinkeby.io/). The Rinkeby Faucet worked for me although the linked website does not show anything and I had to wait about 24 hours before receiving funds.
Once you have funds, you can either deploy the smart contract in `SimpleContract.sol` by running or you can use my contract instance at [`0x106Ca640300A14AB5fcd41A3C9d1C6C1A537B6dE`](https://rinkeby.etherscan.io/address/0x106ca640300a14ab5fcd41a3c9d1c6c1a537b6de). To deploy a new contract instance, run
```
$ ./deploy-contract.bin 0xaa840415.key 'password'
Loaded account with address 0xAa84041541030E1A3cA1dbDf39Ac54e3341C309F
Contract deployed at address 0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx in transaction 0xyyyyyyyy
```
Modify the contract:
```
$ ./modify-contract.bin 0xaa840415.key password 0x106Ca640300A14AB5fcd41A3C9d1C6C1A537B6dE
Loaded account with address 0xAa84041541030E1A3cA1dbDf39Ac54e3341C309F
Connected to wss://rinkeby.infura.io/ws
Old contract number -1
Modified contract number to 1 in transaction 0xzzzzzzzz
```
You can wait for contract events with `wait-for-contract-events.go`:
```
$ ./wait-for-contract-events.bin 0x106Ca640300A14AB5fcd41A3C9d1C6C1A537B6dE
Connecting to wss://rinkeby.infura.io/ws...
Connected successfully to wss://rinkeby.infura.io/ws
2019/07/06 18:48:04 Update by 0x817c86d2. old number -1, new number 1
2019/07/06 18:48:49 Update by 0x817c86d2. old number 1, new number 3
```

For testing purposes with [Ganache](https://www.trufflesuite.com/ganache), you can save the private keys of default Ganache accounts with `save-ganache-keys.go`.



## Acknowledgement

The author acknowledges the convenient websocket access to the Rinkeby network provided by [Infura](https://infura.io):
> Our easy to use API and developer tools provide secure, reliable, and scalable access to Ethereum and IPFS (the [InterPlanetary File System](https://ipfs.io/)). We provide the infrastructure for your decentralized applications so you can focus on the features.

[Ganache](https://www.trufflesuite.com/ganache) was very helpful, too:
> Quickly fire up a personal Ethereum blockchain which you can use to run tests, execute commands, and inspect state while controlling how the chain operates.
