// Copyright 2019 Christoph Conrads
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pragma solidity >=0.4.0 <0.7.0;

contract SimpleContract {
	int64 public number_;

	constructor(int64 number) public {
		number_ = number;
	}

	event Update(address from, int64 old_number, int64 new_number);

	function set(int64 old_number, int64 new_number) public {
		require(number_ == old_number); // avoid race conditions
		require(new_number > 0);

		number_ = new_number;

		emit Update(msg.sender, old_number, new_number);
	}

	function get() public view returns (int64) {
		return number_;
	}
}
