.PHONY: clean

all: \
	deploy-contract.bin \
	make-private-key.bin \
	modify-contract.bin \
	save-ganache-keys.go \
	wait-for-contract-events.bin


%.bin: %.go contracts/SimpleContract.go
	go build -o $@ $< 


contracts/SimpleContract.go: SimpleContract.sol
	mkdir -p contracts
	abigen --sol=$< --pkg=contracts --out=$@


clean:
	$(RM) contracts/SimpleContract.go
