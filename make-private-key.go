// Copyright 2019 Christoph Conrads
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import "fmt"
import "github.com/ethereum/go-ethereum/accounts/keystore"
import "github.com/ethereum/go-ethereum/crypto"
import "log"
import "os"


func main() {
	args := os.Args

	if len(args) != 4 {
		fmt.Fprintf(
			os.Stderr,
			"Usage: go run %s <initializer> <wallet directory> <password>\n",
			args[0])
		os.Exit(1)
	}

	initializer := args[1]
	walletDirectory := args[2]
	password := args[3]


	// derive key pair
	hexKey := crypto.Keccak256Hash([]byte(initializer)).Hex()[2:]
	privateKey, err := crypto.HexToECDSA(hexKey)

	if err != nil {
		log.Fatal(err)
	}


	// save key in keystore
	ks := keystore.NewKeyStore(
		walletDirectory, keystore.StandardScryptN, keystore.StandardScryptP)

	account, err := ks.ImportECDSA(privateKey, password)

	if err != nil {
		log.Fatal(err)
	}

	println("Created account with address", account.Address.Hex())
}
