// Copyright 2019 Christoph Conrads
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import contracts "./contracts"
import "fmt"
import "github.com/ethereum/go-ethereum/common"
import "github.com/ethereum/go-ethereum/ethclient"
import "log"
import "os"


func main() {
	args := os.Args

	if len(args) != 2 {
		fmt.Fprintf(
			os.Stderr,
			"Usage: go run %s <contract address>\n",
			args[0])
		os.Exit(1)
	}


	rawContractAddress := args[1]


	// connect to an ETH websockets gateway
	gatewayUrl := "wss://rinkeby.infura.io/ws"

	fmt.Printf("Connecting to %s...\n", gatewayUrl)

	client, err := ethclient.Dial(gatewayUrl)

	if err != nil {
		log.Fatal(err)
	}


	fmt.Printf("Connected successfully to %s\n", gatewayUrl)


	// locate contract
	contractAddress := common.HexToAddress(rawContractAddress)
	contract, err := contracts.NewSimpleContract(contractAddress, client)

	if err != nil {
		log.Fatal(err)
	}


	// watch events
	filterer := contract.SimpleContractFilterer
	updateChannel := make(chan *contracts.SimpleContractUpdate)
	updateSubscription, err := filterer.WatchUpdate(nil, updateChannel)

	if err != nil {
		log.Fatal(err)
	}

	for {
		select {
		case err := <-updateSubscription.Err():
			log.Fatal(err)

		case event := <-updateChannel:
			modifier := event.From
			oldNumber := event.OldNumber
			newNumber := event.NewNumber

			log.Printf(
				"Update by %s. old number %d, new number %d\n",
				modifier.Hex()[:10], oldNumber, newNumber)
		}
	}
}
